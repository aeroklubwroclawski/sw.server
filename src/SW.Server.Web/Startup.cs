﻿using System;
using System.Diagnostics;
using Microsoft.Owin;
using Owin;
using Serilog;
using Serilog.Exceptions;

[assembly: OwinStartup(typeof(SW.Server.Web.Startup))]

namespace SW.Server.Web
{
  public class Startup
  {
    public void Configuration(IAppBuilder app)
    {
      // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
      Log.Logger = new LoggerConfiguration()
        .Enrich.FromLogContext()
        .Enrich.WithThreadId()
        .Enrich.WithExceptionDetails()
        .WriteTo.Trace()
        .WriteTo.Console()
        .MinimumLevel.Verbose()
        .CreateLogger();

      //Serilog.Debugging.SelfLog.Enable(msg => Trace.Write(msg));
    }
  }
}
