﻿using System;
using Nancy.Hosting.Self;
using Serilog;
using Topshelf;

namespace SW.Server.Service
{
  public class SilentWingsServerServiceHost : ServiceControl
  {
    private NancyHost _nancyHost;

    public bool Start(HostControl hostControl)
    {
      var uri = new Uri("http://localhost:8081");

      _nancyHost = new NancyHost(uri);

      _nancyHost.Start();

      Log.Information("Your application is running on " + uri);

      return true;
    }

    public bool Stop(HostControl hostControl)
    {
      _nancyHost.Stop();
      _nancyHost.Dispose();

      return true;
    }
  }
}