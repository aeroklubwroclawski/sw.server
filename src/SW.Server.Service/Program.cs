﻿using System;
using System.Globalization;
using Serilog;
using Serilog.Exceptions;
using Topshelf;

namespace SW.Server.Service
{
  class Program
  {
    static void Main(string[] args)
    {
      Log.Logger = new LoggerConfiguration()
        .Enrich.FromLogContext()
        .Enrich.WithThreadId()
        .Enrich.WithExceptionDetails()
        .WriteTo.ColoredConsole()
        .WriteTo.RollingFile($@"{AppDomain.CurrentDomain.BaseDirectory}\logs\SW.Server-{{Date}}.log")
        .MinimumLevel.Verbose()
        .CreateLogger();

      CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US");

      HostFactory.Run(x =>
      {
        x.Service<SilentWingsServerServiceHost>();

        x.UseSerilog(Log.Logger);

        x.StartAutomatically();
      });
    }
  }
}
