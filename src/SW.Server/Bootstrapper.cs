﻿using System.Collections.Generic;
using System.Linq;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using Serilog;
using SW.Server.Configuration;
using SW.Server.Serialization;

namespace SW.Server
{
  public class Bootstrapper : DefaultNancyBootstrapper
  {
    protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
    {
      base.ApplicationStartup(container, pipelines);

      Settings.RootPath = RootPathProvider.GetRootPath();

      StaticConfiguration.DisableErrorTraces = false;

      Nancy.Json.JsonSettings.MaxJsonLength = int.MaxValue;
      Nancy.Json.JsonSettings.PrimitiveConverters.Add(new JsonEnumConverter());

      pipelines.BeforeRequest += ctx =>
      {
        var queryParams = ((IDictionary<string, object>) ctx.Request.Query)
          .Select(x => $"{x.Key}={x.Value}");

        Log.Debug("{Verb} {Path}?{Params}", ctx.Request.Method, ctx.Request.Path, string.Join("&", queryParams));

        return null;
      };

      pipelines.AfterRequest += ctx =>
      {
        ctx.Response
          .WithHeader("Access-Control-Allow-Origin", "*")
          .WithHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE")
          .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-Type");
      };

      pipelines.OnError += (ctx, ex) => {
        Log.Error(ex, "Error while processing request");
        return null;
      };
    }
  }
}