﻿using System;
using SW.Server.Domain.Application.Model;
using SW.Server.Domain.Application.Model.Enums;

namespace SW.Server.Seed
{
  public static class SeedData
  {
    public static void Seed()
    {
      var competition = new Competition("SGP Poland 2017", "Sail Plane GP Poland 2017");
      competition.Save();

      var start = DateTime.UtcNow.AddMinutes(5);

      var @event = new Event("Day 1", "First day of SGP Poland 2017", new DateTime(2017, 05, 11), competition.Id);

      var task = new Task("Day 1", start);

      var turnpoint = new Turnpoint("EPWS", TurnpointType.Start, 51.204783, 16.996425, ObservationZone.Line, 2000);
      turnpoint.UpdateMinAltitude(0);
      turnpoint.UpdateMaxAltitude(10000);
      task.AddTurnpoint(turnpoint);

      turnpoint = new Turnpoint("Oborniki Śl", TurnpointType.Turnpoint, 51.300846, 16.916368, ObservationZone.Cylinder, 500);
      turnpoint.UpdateMinAltitude(0);
      turnpoint.UpdateMaxAltitude(10000);
      task.AddTurnpoint(turnpoint);

      turnpoint = new Turnpoint("Trzebnica", TurnpointType.Turnpoint, 51.310290, 17.061250, ObservationZone.Cylinder,500);
      turnpoint.UpdateMinAltitude(0);
      turnpoint.UpdateMaxAltitude(10000);
      task.AddTurnpoint(turnpoint);

      turnpoint = new Turnpoint("Oleśnica", TurnpointType.Turnpoint, 51.216613, 17.388093, ObservationZone.Cylinder, 500);
      turnpoint.UpdateMinAltitude(0);
      turnpoint.UpdateMaxAltitude(10000);
      task.AddTurnpoint(turnpoint);


      turnpoint = new Turnpoint("EPWS", TurnpointType.Finish, 51.210896, 16.994794, ObservationZone.Line, 1000);
      turnpoint.UpdateMinAltitude(0);
      turnpoint.UpdateMaxAltitude(10000);
      task.AddTurnpoint(turnpoint);

      @event.SetTask(task);

      @event.AddTrack(new TrackDescription("OGN215878", "OGN215878", "OGN", "CZE", "Glider", "Bramar"));
      @event.AddTrack(new TrackDescription("OGN1DCD5B", "OGN1DCD5B", "OGN", "CZE", "Glider", "Bramar"));
      @event.AddTrack(new TrackDescription("OGN3F6177", "OGN3F6177", "OGN", "CZE", "Glider", "Bramar"));
      @event.AddTrack(new TrackDescription("OGN296033", "OGN296033", "OGN", "CZE", "Glider", "Bramar"));

      @event.Save();
    }
  }
}