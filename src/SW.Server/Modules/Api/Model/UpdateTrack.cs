﻿namespace SW.Server.Modules.Api.Model
{
  public class UpdateTrack
  {
    public string TrackId { get; set; }

    public string PilotName { get; set; }

    public string CompetitionId { get; set; }

    public string Country { get; set; }

    public string Aircraft { get; set; }

    public string PortraitUrl { get; set; }

    public string Registration { get; set; }

    public string Model { get; set; }

    public string ModelVariant { get; set; }
  }
}