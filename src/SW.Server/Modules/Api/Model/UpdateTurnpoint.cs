﻿using System;

namespace SW.Server.Modules.Api.Model
{
  public class UpdateTurnpoint
  {
    public Guid Id { get; set; }

    public string Name { get; set; }

    public string Type { get; set; }

    public double Latitude { get; set; }

    public double Longitude { get; set; }

    public double MinAltitude { get; set; }

    public double MaxAltitude { get; set; }

    public string ObservationZone { get; set; }

    public double Radius { get; set; }

    public string Trigger { get; set; }

    public double ConeIncline { get; set; }
  }
}