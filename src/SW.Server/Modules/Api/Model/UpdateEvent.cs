﻿using System;
using System.Collections.Generic;

namespace SW.Server.Modules.Api.Model
{
  public class UpdateEvent
  {
    public string Name { get; set; }

    public string Description { get; set; }

    public DateTime StartOpen { get; set; }

    public string BannerUrl { get; set; }

    public UpdateTask Task { get; set; }

    public List<UpdateTrack> Tracks { get; set; }
  }
}