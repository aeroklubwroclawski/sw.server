﻿using System;

namespace SW.Server.Modules.Api.Model
{
  public class CreateTask
  {
    public string Name { get; set; }

    public DateTime StartOpen { get; set; }

    public CreateTurnpoint[] Turnpoints { get; set; }
  }
}