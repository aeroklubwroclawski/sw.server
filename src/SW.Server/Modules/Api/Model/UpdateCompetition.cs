﻿namespace SW.Server.Modules.Api.Model
{
  public class UpdateCompetition
  {
    public string Name { get; set; }

    public string Description { get; set; }

    public string BannerUrl { get; set; }
  }
}