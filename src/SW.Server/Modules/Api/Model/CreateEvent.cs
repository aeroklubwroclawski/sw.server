﻿using System;

namespace SW.Server.Modules.Api.Model
{
  public class CreateEvent
  {
    public string Name { get; set; }

    public string Description { get; set; }

    public DateTime StartOpen { get; set; }

    public string BannerUrl { get; set; }

    public CreateTask Task { get; set; }
  }
}