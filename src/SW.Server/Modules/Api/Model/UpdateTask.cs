﻿using System;

namespace SW.Server.Modules.Api.Model
{
  public class UpdateTask
  {
    public string Name { get; set; }

    public DateTime StartOpen { get; set; }

    public UpdateTurnpoint[] Turnpoints { get; set; }
  }
}