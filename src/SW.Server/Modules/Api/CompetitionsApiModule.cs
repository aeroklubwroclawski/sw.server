﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nancy;
using Nancy.ModelBinding;
using Serilog;
using SW.Server.Domain.Application.Model;
using SW.Server.Domain.Application.Model.Enums;
using SW.Server.Modules.Api.Model;

namespace SW.Server.Modules.Api
{
  public class CompetitionsApiModule : NancyModule
  {
    public CompetitionsApiModule()
      : base("/api/competitions")
    {
      Get["/"] = _ => Response.AsJson(Competition.GetAll());
      Get["/{id}"] = GetCompetition;

      Get["/{id}/events"] = GetCompetitionEvents;
      Get["/{id}/events/{eventId}"] = GetEvent;

      Put["/{id}"] = UpdateCompetition;
      Put["/{id}/events/{eventId}"] = UpdateEvent;

      Post["/"] = CreateCompetition;
      Post["/{id}/events"] = CreateEvent;

      Delete["/{id}"] = DeleteCompetition;
      Delete["/{id}/events/{eventId}"] = DeleteEvent;
    }

    private dynamic GetCompetition(dynamic parameters)
    {
      Competition competition = Competition.Get(parameters.id);
      return Response.AsJson(competition);
    }

    private dynamic GetCompetitionEvents(dynamic parameters)
    {
      Competition competition = Competition.Get(parameters.id);

      var result = new List<Event>();

      foreach (var competitionEvent in competition.Events)
      {
        result.Add(Event.Get(competitionEvent.Id));
      }

      return Response.AsJson(result);
    }

    private dynamic CreateCompetition(dynamic _)
    {
      var request = this.Bind<CreateCompetiton>();

      var competition = new Competition(request.Name, request.Description);

      competition.UpdateBannerUrl(request.BannerUrl);

      competition.Save();

      return Response.AsJson(competition, HttpStatusCode.Created);
    }

    private dynamic UpdateCompetition(dynamic parameters)
    {
      var request = this.Bind<UpdateCompetition>();

      var competition = Competition.Get(parameters.id);

      competition.UpdateName(request.Name);
      competition.UpdateDescription(request.Description);
      competition.UpdateBannerUrl(request.BannerUrl);

      competition.Save();

      return HttpStatusCode.NoContent;
    }

    private dynamic DeleteCompetition(dynamic parameters)
    {
      var competition = Competition.Get(parameters.id);

      competition.Delete();

      return HttpStatusCode.NoContent;
    }

    private dynamic GetEvent(dynamic parameters)
    {
      Event @event = Event.Get(parameters.eventId);

      return Response.AsJson(@event);
    }

    private dynamic CreateEvent(dynamic parameters)
    {
      var request = this.Bind<CreateEvent>();

      var @event = new Event(request.Name, request.Description, request.StartOpen, parameters.id);
      var task = new Task(request.Task?.Name, request.Task?.StartOpen ?? DateTime.Now);

      var turnpoints = request.Task?.Turnpoints.Select(x => Turnpoint.Create(
        x.Name,
        x.Type,
        x.Latitude,
        x.Longitude,
        x.MinAltitude,
        x.MaxAltitude,
        x.ObservationZone,
        x.Radius,
        x.Trigger,
        x.ConeIncline));

      if (turnpoints != null)
      {
        foreach (var turnpoint in turnpoints)
        {
          task.AddTurnpoint(turnpoint);
        }
      }

      @event.UpdateBannerUrl(request.BannerUrl);
      @event.SetTask(task);

      @event.Save();

      return Response.AsJson(@event, HttpStatusCode.Created);
    }

    private dynamic UpdateEvent(dynamic parameters)
    {
      var request = this.Bind<UpdateEvent>();

      Event @event = Event.Get(parameters.eventId);
      Task task = @event.Task;

      @event.UpdateName(request.Name);
      @event.UpdateDescription(request.Description);
      @event.UpdateBannerUrl(request.BannerUrl);
      @event.UpdateStartOpen(request.StartOpen);

      task.UpdateName(request.Task.Name);
      task.UpdateStartOpen(request.Task.StartOpen);

      task.ClearTurnpoints();

      foreach (var updateTurnpoint in request.Task.Turnpoints)
      {
        var newTurnpoint = Turnpoint.Create(
          updateTurnpoint.Name,
          updateTurnpoint.Type,
          updateTurnpoint.Latitude,
          updateTurnpoint.Longitude,
          updateTurnpoint.MinAltitude,
          updateTurnpoint.MaxAltitude,
          updateTurnpoint.ObservationZone,
          updateTurnpoint.Radius,
          updateTurnpoint.Trigger,
          updateTurnpoint.ConeIncline);

        task.AddTurnpoint(newTurnpoint);
      }

      @event.ClearTracks();

      foreach (var updateTrack in request.Tracks)
      {
        var trackDesription = new TrackDescription(
          updateTrack.TrackId,
          updateTrack.PilotName,
          updateTrack.CompetitionId,
          updateTrack.Country,
          updateTrack.Aircraft,
          updateTrack.ModelVariant);

        trackDesription.SetPortraitUrl(updateTrack.PortraitUrl);
        trackDesription.SetRegistration(updateTrack.Registration);

        @event.AddTrack(trackDesription);
      }

      Log.Debug("Event to save: {@event}", @event);

      @event.Save();

      return HttpStatusCode.NoContent;
    }

    private dynamic DeleteEvent(dynamic parameters)
    {
      var competition = Competition.Get(parameters.id);

      competition.RemoveEvent(parameters.eventId);

      competition.Save();

      return HttpStatusCode.NoContent;
    }
  }
}