﻿using Nancy;

namespace SW.Server.Modules.Web
{
  public class IndexModule : NancyModule
  {
    public IndexModule()
    {
      Get["/"] = _ =>
      {
        return View["index.html"];
      };
    }
  }
}