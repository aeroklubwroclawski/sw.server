﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nancy;
using Nancy.ModelBinding;
using Newtonsoft.Json;
using Serilog;
using SW.Server.Configuration;
using SW.Server.Domain.Application.DataAccess;
using SW.Server.Domain.Application.DataAccess.Model;
using SW.Server.Domain.Application.Model;
using SW.Server.Domain.SilentWings.Converters;
using SW.Server.Modules.SilentWingsStudio.RequestObjects;

namespace SW.Server.Modules.SilentWingsStudio
{
  public class SilentWingsStudioModule : NancyModule
  {
    public SilentWingsStudioModule()
    {
      Get["/eventgroups"] = _ =>
      {
        var competitions = Competition.GetAll();

        var eventGroups = competitions.Select(CompetitionConverter.ToEventGroup).ToList();

        Log.Debug("Event groups {@group}", eventGroups);

        return Response.AsJson(eventGroups);
      };

      Get["/event"] = _ =>
      {
        var requestObject = this.Bind<EventRequest>();

        var @event = Event.Get(requestObject.EventId);

        if (Settings.UseTrackpointSqlSearch)
        {
          @event.ClearTracks();

          var callsigns = GetCallsignsFromDatabase(@event.StartOpen);

          foreach (var callsign in callsigns)
          {
            @event.AddTrack(new TrackDescription(callsign, callsign, "OGN", "POL", "Glider", "Bramar"));
          }
        }

        var result = EventConverter.ToSilentWingsEvent(@event);

        Log.Debug("Event {@Result}", result);

        return Response.AsJson(result);
      };

      Get["/trackpoints"] = _ =>
      {
        var requestObject = this.Bind<TrackpointsRequest>();

        var @event = Event.Get(new Guid(requestObject.EventId));

        var dateBase = @event.StartOpen;

        var startDate = dateBase.Date;
        var endDate = dateBase.Date.AddHours(24).AddSeconds(-1);

        var beginDate = requestObject.Begin == 0 ? startDate : requestObject.BeginDate;

        Log.Debug("Request: {@request}", requestObject);
        Log.Debug("Looking for trackpoints between {from} and {to}", beginDate, endDate);

        List<TrackingPoint> points;

        using (var context = new AprsContext())
        {
          points = context.TrackingPoints
            .Where(x => x.Callsign == requestObject.TrackId)
            .Where(x => x.Time >= beginDate)
            .Where(x => x.Time < endDate)
            .OrderBy(x => x.Time)
            .ToList();
        }

        var coordinates = points.Select(x => new Domain.SilentWings.Model.Trackpoint
          {
            Latitude = x.Latitude,
            Longitude = x.Longitude,
            Altitude = x.Altitude,
            Timestamp = new DateTimeOffset(x.Time).ToUnixTimeSeconds()
          })
          .OrderBy(x => x.Timestamp)
          .ToList();

        var trackpoint = new Domain.SilentWings.Model.Track
        {
          Live = true,
          EventRevision = @event.Revision,
          TrackId = requestObject.TrackId,
          Tracks = coordinates
        };

        var result = JsonConvert.SerializeObject(trackpoint);

        return Response.AsText(result);
      };
    }

    private List<string> GetCallsignsFromDatabase(DateTime eventStartOpen)
    {
      List<string> result;

      using (var context = new AprsContext())
      {
        var dateBase = eventStartOpen;

        var startDate = dateBase.Date;
        var endDate = dateBase.Date.AddHours(24).AddSeconds(-1);

        Log.Debug("Looking for callsigns between {from} and {to}", startDate, endDate);

        result = context.TrackingPoints
          .Where(x => x.Time >= startDate)
          .Where(x => x.Time < endDate)
          .Where(x => x.Callsign.Length == 9)
          .Where(x => x.Latitude > 50.823053 && x.Latitude < 51.814553)
          .Where(x => x.Longitude > 15.978990 && x.Longitude < 17.554183)
          .Select(x => x.Callsign)
          .Distinct()
          .ToList();

        Log.Debug("Found {count} callsigns", result.Count);
      }

      return result;
    }
  }
}