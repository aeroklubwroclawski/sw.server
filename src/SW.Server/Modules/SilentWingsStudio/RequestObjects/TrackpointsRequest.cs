﻿using System;

namespace SW.Server.Modules.SilentWingsStudio.RequestObjects
{
  public class TrackpointsRequest
  {
    public string TrackId { get; set; }

    public string EventId { get; set; }

    public int Begin { get; set; }

    public DateTime BeginDate => DateTimeOffset.FromUnixTimeSeconds(Begin).DateTime;
  }
}