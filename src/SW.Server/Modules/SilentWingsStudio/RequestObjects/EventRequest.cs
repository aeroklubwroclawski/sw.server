﻿using System;

namespace SW.Server.Modules.SilentWingsStudio.RequestObjects
{
  public class EventRequest
  {
    public Guid EventId { get; set; }
  }
}