﻿using System.Configuration;

namespace SW.Server.Configuration
{
  public static class Settings
  {
    public static string RootPath { get; set; }

    public static bool UseTrackpointSqlSearch => bool.Parse(ConfigurationManager.AppSettings["UseTrackpointSqlSearch"]);
  }
}