﻿namespace SW.Server.Domain.Application.Model
{
  public class Trackpoint
  {
    public double Timestamp { get; private set; }

    public double Latitude { get; private set; }

    public double Longitude { get; private set; }

    public double Altitude { get; private set; }

    public Trackpoint(double timestamp, double latitude, double longitude, double altitude)
    {
      Timestamp = timestamp;
      Latitude = latitude;
      Longitude = longitude;
      Altitude = altitude;
    }
  }
}