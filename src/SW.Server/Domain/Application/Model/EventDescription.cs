﻿using System;

namespace SW.Server.Domain.Application.Model
{
  public class EventDescription
  {
    public Guid Id { get; private set; }

    public DateTime StartOpen { get; private set; }

    public double StartOpenTs => new DateTimeOffset(StartOpen).ToUnixTimeSeconds();

    public EventDescription(Guid id, DateTime startOpen)
    {
      Id = id;
      StartOpen = startOpen;
    }

    public void SetStartOpen(DateTime startOpen)
    {
      StartOpen = startOpen;
    }
  }
}