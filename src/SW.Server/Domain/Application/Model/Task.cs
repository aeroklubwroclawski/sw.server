﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SW.Server.Domain.Application.Model
{
  public class Task
  {
    public string Name { get; private set; }

    public string Type => "SailplaneGradPrix";

    public DateTime StartOpen { get; private set; }

    [JsonIgnore]
    public double StartOpenTs => new DateTimeOffset(StartOpen).ToUnixTimeSeconds();

    public List<Turnpoint> Turnpoints { get; private set; }

    public Task(string name, DateTime startOpen)
    {
      Name = name;
      StartOpen = startOpen;
      Turnpoints = new List<Turnpoint>();
    }

    [JsonConstructor]
    public Task(string name, DateTime startOpen, List<Turnpoint> turnpoints)
      :this(name, startOpen)
    {
      Turnpoints = turnpoints;
    }

    public void UpdateName(string name)
    {
      Name = name;
    }

    public void UpdateStartOpen(DateTime startOpen)
    {
      StartOpen = startOpen;
    }

    public void AddTurnpoint(Turnpoint turnpoint)
    {
      Turnpoints.Add(turnpoint);
    }

    public void ClearTurnpoints()
    {
      Turnpoints.Clear();
    }
  }
}