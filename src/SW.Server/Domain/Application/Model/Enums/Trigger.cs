﻿namespace SW.Server.Domain.Application.Model.Enums
{
  public enum Trigger
  {
    Enter,
    Exit
  }
}