﻿namespace SW.Server.Domain.Application.Model.Enums
{
  public enum ObservationZone
  {
    Cylinder,
    Sector,
    Line,
    Cone
  }
}