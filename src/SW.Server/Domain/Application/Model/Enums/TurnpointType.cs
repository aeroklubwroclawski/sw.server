﻿namespace SW.Server.Domain.Application.Model.Enums
{
  public enum TurnpointType
  {
    Takeoff,
    Start,
    StartOfSpeedSection,
    Turnpoint,
    EndOfSpeedSection,
    Finish,
    Landing
  }
}