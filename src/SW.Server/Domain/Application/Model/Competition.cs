﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using SW.Server.Configuration;

namespace SW.Server.Domain.Application.Model
{
  public class Competition
  {
    private const string DataPath = "./data/competitions";

    private static readonly string CompetitionsDataPath = Path.Combine(Settings.RootPath, DataPath);

    #region Properties

    public Guid Id { get; private set; }

    public string Name { get; private set; }

    public string Description { get; private set; }

    public string BannerUrl { get; private set; }

    public List<EventDescription> Events { get; private set; }

    #endregion

    #region Constructors

    static Competition()
    {
      EnsureDataDirectoryCreated();
    }

    public Competition(string name, string description)
    {
      Id = Guid.NewGuid();
      Name = name;
      Description = description;
      Events = new List<EventDescription>();
    }

    [JsonConstructor]
    protected Competition(Guid id, string name, string description, string bannerUrl, List<EventDescription> events)
      : this(name, description)
    {
      Id = id;
      BannerUrl = bannerUrl;
      Events = events;
    }

    #endregion

    #region Methods

    public static List<Competition> GetAll()
    {
      var competitionFiles = Directory.EnumerateFiles(CompetitionsDataPath);

      List<Competition> result = new List<Competition>();

      foreach (var competitionFile in competitionFiles)
      {
        var competition = JsonConvert.DeserializeObject<Competition>(File.ReadAllText(competitionFile));

        result.Add(competition);
      }

      return result;
    }

    public static Competition Get(Guid id)
    {
      return JsonConvert.DeserializeObject<Competition>(File.ReadAllText($"{CompetitionsDataPath}/{id}.json"));
    }

    public void UpdateName(string name)
    {
      Name = name;
    }

    public void UpdateDescription(string description)
    {
      Description = description;
    }

    public void UpdateBannerUrl(string bannerUrl)
    {
      BannerUrl = bannerUrl;
    }

    public void Save()
    {
      var jsonContent = JsonConvert.SerializeObject(this, Formatting.Indented);

      using (var file = File.Create($"{CompetitionsDataPath}/{Id}.json"))
      using (var writer = new StreamWriter(file))
      {
        writer.Write(jsonContent);
      }
    }

    public void Delete()
    {
      File.Delete($"{CompetitionsDataPath}/{Id}.json");
    }

    public void AddEvent(EventDescription eventDescription)
    {
      Events.Add(eventDescription);
    }

    public void RemoveEvent(Guid id)
    {
      Events.RemoveAll(x => x.Id == id);
    }

    #endregion

    private static void EnsureDataDirectoryCreated()
    {
      if (Directory.Exists(CompetitionsDataPath) == false)
      {
        Directory.CreateDirectory(CompetitionsDataPath);
      }
    }
  }
}