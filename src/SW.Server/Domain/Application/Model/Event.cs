﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using SW.Server.Configuration;

namespace SW.Server.Domain.Application.Model
{
  public class Event
  {
    private const string DataPath = "./data/events";

    private static readonly string EventsDataPath = Path.Combine(Settings.RootPath, DataPath);

    #region Properties

    public Guid Id { get; private set; }

    public DateTime StartOpen { get; private set; }

    [JsonIgnore]
    public double StartOpenTs => new DateTimeOffset(StartOpen).ToUnixTimeSeconds();

    public string Name { get; private set; }

    public string Description { get; private set; }

    public string BannerUrl { get; private set; }

    public int Revision { get; private set; }

    public Guid CompetitionId { get; private set; }

    public Task Task { get; private set; }

    public List<TrackDescription> Tracks { get; private set; }

    #endregion

    static Event()
    {
      EnsureDataDirectoryCreated();
    }

    public Event(string name, string description, DateTime startOpen, Guid competitionId)
    {
      Id = Guid.NewGuid();
      Name = name;
      Description = description;
      StartOpen = startOpen;
      CompetitionId = competitionId;
      Tracks = new List<TrackDescription>();
    }

    [JsonConstructor]
    protected Event(Guid id, DateTime startOpen, string name, string description, string bannerUrl, int revision, Guid competitionId, Task task)
      : this(name, description, startOpen, competitionId)
    {
      Id = id;
      BannerUrl = bannerUrl;
      Revision = revision;
      Task = task;
    }

    #region Persistence

    public static Event Get(Guid id)
    {
      return JsonConvert.DeserializeObject<Event>(File.ReadAllText($"{EventsDataPath}/{id}.json"));
    }

    public void Save()
    {
      var jsonContent = JsonConvert.SerializeObject(this, Formatting.Indented);

      using (var file = File.Create($"{EventsDataPath}/{Id}.json"))
      using (var writer = new StreamWriter(file))
      {
        writer.Write(jsonContent);
      }

      var competition = Competition.Get(CompetitionId);

      var eventDesctiption = competition.Events.FirstOrDefault(x => x.Id == Id);

      if (eventDesctiption == null)
      {
        competition.AddEvent(new EventDescription(Id, StartOpen));
      }
      else
      {
        eventDesctiption.SetStartOpen(StartOpen);
      }

      competition.Save();
    }

    public void Delete()
    {
      var competition = Competition.Get(CompetitionId);

      competition.RemoveEvent(Id);
      competition.Save();

      File.Delete($"{EventsDataPath}/{Id}.json");
    }

    #endregion

    public void UpdateName(string name)
    {
      Name = name;
      UpdateRevision();
    }

    public void UpdateDescription(string description)
    {
      Description = description;
      UpdateRevision();
    }

    public void UpdateBannerUrl(string bannerUrl)
    {
      BannerUrl = bannerUrl;
      UpdateRevision();
    }

    public void UpdateStartOpen(DateTime startOpen)
    {
      StartOpen = startOpen;
      UpdateRevision();
    }

    public void SetTask(Task task)
    {
      Task = task;
    }

    public void AddTrack(TrackDescription trackDescription)
    {
      Tracks.Add(trackDescription);
    }

    public void ClearTracks()
    {
      Tracks.Clear();
    }

    private void UpdateRevision()
    {
      Revision++;
    }

    private static void EnsureDataDirectoryCreated()
    {
      if (Directory.Exists(EventsDataPath) == false)
      {
        Directory.CreateDirectory(EventsDataPath);
      }
    }
  }
}