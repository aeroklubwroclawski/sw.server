﻿using System;
using Newtonsoft.Json;
using SW.Server.Domain.Application.Model.Enums;

namespace SW.Server.Domain.Application.Model
{
  public class Turnpoint
  {
    public Guid Id { get; private set; }

    public string Name { get; private set; }

    public TurnpointType Type { get; private set; }

    public double Latitude { get; private set; }

    public double Longitude { get; private set; }

    public double MinAltitude { get; private set; }

    public double MaxAltitude { get; private set; }

    public ObservationZone ObservationZone { get; private set; }

    public double Radius { get; private set; }

    public Trigger Trigger { get; private set; }

    public double ConeIncline { get; private set; }

    public Turnpoint(string name, TurnpointType type, double latitude, double longitude, ObservationZone observationZone, double radius)
    {
      Id = Guid.NewGuid();
      Name = name;
      Type = type;
      Latitude = latitude;
      Longitude = longitude;
      ObservationZone = observationZone;
      Radius = radius;
    }

    [JsonConstructor]
    protected Turnpoint(string name, TurnpointType type, double latitude, double longitude, double minAltitude, double maxAltitude, ObservationZone observationZone, double radius, Trigger trigger, double coneIncline)
      : this(name, type, latitude, longitude, observationZone, radius)
    {
      MinAltitude = minAltitude;
      MaxAltitude = maxAltitude;
      Trigger = trigger;
      ConeIncline = coneIncline;
    }

    public static Turnpoint Create(string name, string type, double latitude, double longitude, double minAltitude, double maxAltitude, string observationZone, double radius, string trigger, double coneIncline)
    {
      TurnpointType turnpointType;
      ObservationZone turnpointObservationZone;
      Trigger turnpointTrigger;

      Enum.TryParse(type, out turnpointType);
      Enum.TryParse(observationZone, out turnpointObservationZone);
      Enum.TryParse(trigger, out turnpointTrigger);

      return new Turnpoint(name, turnpointType, latitude, longitude, minAltitude, maxAltitude, turnpointObservationZone, radius, turnpointTrigger, coneIncline);
    }

    public void UpdateName(string name)
    {
      Name = name;
    }

    public void UpdateType(TurnpointType type)
    {
      Type = type;
    }

    public void UpdateLatitude(double latitude)
    {
      Latitude = latitude;
    }

    public void UpdateLongitude(double longitude)
    {
      Longitude = longitude;
    }

    public void UpdateMinAltitude(double minAltitude)
    {
      MinAltitude = minAltitude;
    }

    public void UpdateMaxAltitude(double maxAltitude)
    {
      MaxAltitude = maxAltitude;
    }

    public void UpdateObservationZone(ObservationZone observationZone)
    {
      ObservationZone = observationZone;
    }

    public void UpdateRadius(double radius)
    {
      Radius = radius;
    }

    public void UpdateTrigger(Trigger trigger)
    {
      Trigger = trigger;
    }

    public void UpdateConeIncline(double coneIncline)
    {
      ConeIncline = coneIncline;
    }
  }
}