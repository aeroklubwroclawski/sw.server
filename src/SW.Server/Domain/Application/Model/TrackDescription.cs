﻿using Newtonsoft.Json;

namespace SW.Server.Domain.Application.Model
{
  public class TrackDescription
  {
    public string TrackId { get; private set; }

    public string PilotName { get; private set; }

    public string CompetitionId { get; private set; }

    public string Country { get; private set; }

    public string Aircraft { get; private set; }

    public string PortraitUrl { get; private set; }

    public string Registration { get; private set; }

    [JsonProperty("3dModel")]
    public string Model { get; private set; }

    [JsonProperty("3dModelVariant")]
    public string ModelVariant { get; private set; }

    public string[] RibbonColors { get; private set; }

    public TrackDescription(string trackId, string pilotName, string competitionId, string country, string aircraft, string modelVariant)
    {
      TrackId = trackId;
      PilotName = pilotName;
      CompetitionId = competitionId;
      Country = country;
      Aircraft = aircraft;
      RibbonColors = new string[0];
      Model = "ventus2";
      ModelVariant = modelVariant;
    }

    [JsonConstructor]
    protected TrackDescription(string trackId, string pilotName, string competitionId, string country, string aircraft, string portraitUrl, string registration, string model, string modelVariant, string[] ribbonColors)
    {
      TrackId = trackId;
      PilotName = pilotName;
      CompetitionId = competitionId;
      Country = country;
      Aircraft = aircraft;
      PortraitUrl = portraitUrl;
      Registration = registration;
      Model = model;
      ModelVariant = modelVariant;
      RibbonColors = ribbonColors;
    }

    public void SetPortraitUrl(string portraintUrl)
    {
      PortraitUrl = portraintUrl;
    }

    public void SetRegistration(string registration)
    {
      Registration = registration;
    }

    public void SetModel(string model)
    {
      Model = model;
    }

    public void SetModeVariant(string modelVariant)
    {
      ModelVariant = modelVariant;
    }

    public void SetRibbonColors(string color1, string color2, string color3)
    {
      RibbonColors = new[] { color1, color2, color3 };
    }
  }
}