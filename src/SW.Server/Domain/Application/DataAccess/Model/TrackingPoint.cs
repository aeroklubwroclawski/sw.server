﻿using System;

namespace SW.Server.Domain.Application.DataAccess.Model
{
  public class TrackingPoint
  {
    public Guid Id { get; set; }

    public string Callsign { get; set; }

    public DateTime Time { get; set; }

    public float Latitude { get; set; }

    public float Longitude { get; set; }

    public float Altitude { get; set; }
  }
}