﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using SW.Server.Domain.Application.DataAccess.Model;

namespace SW.Server.Domain.Application.DataAccess
{
  public class AprsContext : DbContext
  {
    public AprsContext()
      : base("APRS.Client")
    {
      Database.SetInitializer(new NullDatabaseInitializer<AprsContext>());
      ((IObjectContextAdapter) this).ObjectContext.CommandTimeout = 180;
    }

    public virtual IDbSet<TrackingPoint> TrackingPoints { get; set; }
  }
}