﻿using Newtonsoft.Json;

namespace SW.Server.Domain.SilentWings.Model
{
  public class Turnpoint
  {
    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("type")]
    public string Type { get; set; }

    [JsonProperty("latitude")]
    public double Latitude { get; set;  }

    [JsonProperty("longitude")]
    public double Longitude { get; set; }

    [JsonProperty("minAltitude")]
    public double MinAltitude { get; set; }

    [JsonProperty("maxAltitude")]
    public double MaxAltitude { get; set; }

    [JsonProperty("observationZone")]
    public string ObservationZone { get; set; }

    [JsonProperty("radius")]
    public double Radius { get; set; }

    [JsonProperty("trigger")]
    public string Trigger { get; set; } = "Enter";

    [JsonProperty("coneIncline")]
    public double ConeIncline { get; set; }
  }
}