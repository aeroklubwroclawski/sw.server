﻿using Newtonsoft.Json;

namespace SW.Server.Domain.SilentWings.Model
{
  public class EventDescription
  {
    [JsonProperty("id")]
    public string Id { get; set; }

    [JsonProperty("startOpenTs")]
    public int StartOpenTs { get; set; }
  }
}