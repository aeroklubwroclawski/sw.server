﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SW.Server.Domain.SilentWings.Model
{
  public class EventGroup
  {
    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("description")]
    public string Description { get; set; }

    [JsonProperty("bannnerUrl")]
    public string BannerUrl { get; set; }

    [JsonProperty("events")]
    public List<EventDescription> Events { get; set; }
  }
}