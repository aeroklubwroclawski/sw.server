﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SW.Server.Domain.SilentWings.Model
{
  public class Track
  {
    [JsonProperty("trackId")]
    public string TrackId { get; set; }

    [JsonProperty("live")]
    public bool Live { get; set; }

    [JsonProperty("eventRevision")]
    public int EventRevision { get; set; }

    [JsonProperty("track")]
    public List<Trackpoint> Tracks { get; set; }
  }
}