﻿using Newtonsoft.Json;

namespace SW.Server.Domain.SilentWings.Model
{
  public class TrackDescription
  {
    [JsonProperty("trackId")]
    public string TrackId { get; set; }

    [JsonProperty("pilotName")]
    public string PilotName { get; set; }

    [JsonProperty("competitionId")]
    public string CompetitionId { get; set; }

    [JsonProperty("country")]
    public string Country { get; set; }

    [JsonProperty("aircraft")]
    public string Aircraft { get; set; }

    [JsonProperty("portraitUrl")]
    public string PortraitUrl { get; set; }

    [JsonProperty("registration")]
    public string Registration { get; set; }

    [JsonProperty("3dModel")]
    public string Model { get; set; }

    [JsonProperty("3dModelVariant")]
    public string ModelVariant { get; set; }

    [JsonProperty("ribbonColors")]
    public string[] RibbonColors { get; set; }
  }
}