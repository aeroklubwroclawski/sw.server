﻿using Newtonsoft.Json;

namespace SW.Server.Domain.SilentWings.Model
{
  public class Trackpoint
  {
    [JsonProperty("t")]
    public double Timestamp { get; set; }

    [JsonProperty("n")]
    public double Latitude { get; set; }

    [JsonProperty("e")]
    public double Longitude { get; set; }

    [JsonProperty("a")]
    public double Altitude { get; set; }
  }
}