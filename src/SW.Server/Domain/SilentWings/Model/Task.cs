﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SW.Server.Domain.SilentWings.Model
{
  public class Task
  {
    [JsonProperty("taskName")]
    public string TaskName { get; set; }

    [JsonProperty("taskType")]
    public string TaskType => "SailplaneGrandPrix";

    [JsonProperty("startOpenTs")]
    public int StartOpenTs { get; set; }

    [JsonProperty("turnpoints")]
    public List<Turnpoint> Turnpoints { get; set; }
  }
}