﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SW.Server.Domain.SilentWings.Model
{
  public class Event
  {
    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("description")]
    public string Description { get; set; }

    [JsonProperty("bannerUrl")]
    public string BannerUrl { get; set; }

    [JsonProperty("eventRevision")]
    public int EventRevision { get; set; }

    [JsonProperty("task")]
    public Task Task { get; set; }

    [JsonProperty("tracks")]
    public List<TrackDescription> Tracks { get; set; }
  }
}