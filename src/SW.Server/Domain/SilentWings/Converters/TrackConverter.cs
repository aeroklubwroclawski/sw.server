﻿namespace SW.Server.Domain.SilentWings.Converters
{
  public static class TrackConverter
  {
    public static Model.TrackDescription ToSilentWingsTrack(Application.Model.TrackDescription trackDescription)
    {
      return new Model.TrackDescription
      {
        TrackId = trackDescription.TrackId,
        PilotName = trackDescription.PilotName,
        CompetitionId = trackDescription.CompetitionId,
        Country = trackDescription.Country,
        Aircraft = trackDescription.Aircraft,
        PortraitUrl = trackDescription.PortraitUrl,
        Registration = trackDescription.Registration,
        Model = trackDescription.Model,
        ModelVariant = trackDescription.ModelVariant,
        RibbonColors = trackDescription.RibbonColors
      };
    }
  }
}