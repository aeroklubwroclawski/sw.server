﻿namespace SW.Server.Domain.SilentWings.Converters
{
  public static class EventDescriptionConverter
  {
    public static Model.EventDescription ToSilentWingsEvent(Application.Model.EventDescription eventDescription)
    {
      return new Model.EventDescription
      {
        Id = eventDescription.Id.ToString(),
        StartOpenTs = (int)eventDescription.StartOpenTs
      };
    }
  }
}