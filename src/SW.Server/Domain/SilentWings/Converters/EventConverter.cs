﻿using System.Linq;
using SW.Server.Domain.SilentWings.Model;

namespace SW.Server.Domain.SilentWings.Converters
{
  public static class EventConverter
  {
    public static Event ToSilentWingsEvent(Application.Model.Event @event)
    {
      return new Event
      {
        Name = @event.Name,
        Description = @event.Description,
        BannerUrl = @event.BannerUrl,
        EventRevision = @event.Revision,
        Task = TaskConverter.ToSilentWingsTask(@event.Task),
        Tracks = @event.Tracks.Select(TrackConverter.ToSilentWingsTrack).ToList()
      };
    }
  }
}