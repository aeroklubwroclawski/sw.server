﻿using System.Linq;
using SW.Server.Domain.SilentWings.Model;

namespace SW.Server.Domain.SilentWings.Converters
{
  public static class TaskConverter
  {
    public static Task ToSilentWingsTask(Application.Model.Task task)
    {
      return new Task
      {
        TaskName = task.Name,
        StartOpenTs = (int) task.StartOpenTs,
        Turnpoints = task.Turnpoints.Select(TurnpointConverter.ToSilentWingsTurnpoint).ToList()
      };
    }
  }
}