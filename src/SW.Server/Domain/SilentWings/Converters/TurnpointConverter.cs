﻿namespace SW.Server.Domain.SilentWings.Converters
{
  public static class TurnpointConverter
  {
    public static Model.Turnpoint ToSilentWingsTurnpoint(Application.Model.Turnpoint turnpoint)
    {
      return new Model.Turnpoint
      {
        Name = turnpoint.Name,
        Type = turnpoint.Type.ToString(),
        Latitude = turnpoint.Latitude,
        Longitude = turnpoint.Longitude,
        MinAltitude = turnpoint.MinAltitude,
        MaxAltitude = turnpoint.MaxAltitude,
        ObservationZone = turnpoint.ObservationZone.ToString(),
        Radius = turnpoint.Radius,
        Trigger = turnpoint.Trigger.ToString(),
        ConeIncline = turnpoint.ConeIncline
      };
    }
  }
}