﻿using System.Linq;
using SW.Server.Domain.Application.Model;

namespace SW.Server.Domain.SilentWings.Converters
{
  public static class CompetitionConverter
  {
    public static Model.EventGroup ToEventGroup(Competition competition)
    {
      return new Model.EventGroup
      {
        Name = competition.Name,
        Description = competition.Description,
        BannerUrl = competition.BannerUrl,
        Events = competition.Events.Select(EventDescriptionConverter.ToSilentWingsEvent).ToList()
      };
    }
  }
}